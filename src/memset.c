#include "lib.h"

void *
memset(s,c,n)
	void *s;
	int c;
	size_t n;
{
	int limit;
	if (__lib_work)
	{
		limit=frameSpace(s);
//		printf("limit %d\n",limit);
		if ((limit!=-1) && (limit<n))
		{
			printf("memset: BOF DETECTED INSIDE - %x\n",frameSpace(s));
			/* TODO HOW ACT ? */
			return ___memset(s,c,frameSpace(s));
		}
	}
//	printf("strlen ok \n");
	return ___memset(s,c,n);
}
