#include "lib.h"

/* TODO this must be an struct */
int __lib_work=0;
/* TODO getenv and read config_file */
/* ACTIONS:
 * - stop and show inform
 * - solve and inform
 * - silent solve
 */


/* Symbols */
int  * (*___sysopen)(const char *path,int flags, mode_t mode);
void * (*___memcpy) (void*,const char *,size_t);
void * (*___memset) (void*,int,size_t);
size_t (*___strlen) (const char *);
size_t (*___strnlen)(const char *,size_t);
void * (*___malloc) (size_t);
void * (*___calloc) (size_t,size_t);

static void _libwrap_init() __attribute__ ((constructor));
static void _libwrap_init()
{
	___sysopen=dlsym(RTLD_NEXT,"open");
	___memcpy=dlsym(RTLD_NEXT,"memcpy");
	___strlen=dlsym(RTLD_NEXT,"strlen");
	___memset=dlsym(RTLD_NEXT,"memset");
	___strnlen=dlsym(RTLD_NEXT,"strnlen");
	___malloc=dlsym(RTLD_NEXT,"malloc");
	___calloc=dlsym(RTLD_NEXT,"calloc");
		__lib_work=1;
	if (___memcpy&&___strlen)
	{
		printf("LibSFP initialized.\n");
		__lib_work=1;
	} else {
		printf("Error loading LibSFP.\n");
		exit(1);
	}
}

static void _libwrap_fini() __attribute__ ((destructor));
static void _libwrap_fini() 
{
	if (__lib_work)
	{
//	dlclose(___memcpy);
	}
}
		
void *
getFrame(n)
	int n;
{
	switch(n)
	{
	case 0:
		return __builtin_frame_address(0);
	case 1:
		return __builtin_frame_address(1);
	case 2:
		return __builtin_frame_address(2);
	case 3:
		return __builtin_frame_address(3);
	case 4:
		return __builtin_frame_address(4);
	case 5:
		return __builtin_frame_address(5);
	case 6:
		return __builtin_frame_address(6);
	case 7:
		return __builtin_frame_address(7);
	case 8:
		return __builtin_frame_address(8);
	case 9:
		return __builtin_frame_address(9);
	case 10:
		return __builtin_frame_address(10);
	}
	return 0;
}

int
isStack(ptr)
	void *ptr;
{
	long p=(long)ptr;
	p&=0xFf000000; /* XXX only X86 */

	if (p==0xBf000000)
			return 1;
	return 0;
}

int
stackLevel(ptr)
	void *ptr;
{
	int i=0;
	while((long)getFrame(i)<(long)ptr)
		i++;
	return i-2; // 2 stack levels :) (stackLevel + getFrame)
}

#if 0
int
isOverflow(ptr)
	void *ptr;
{ 
	if (isStack(ptr))
	{
		if ((long)(ptr+n+14) > getFrame(2+stackLevel(dest)))
			return 1;
		else return 0;
	}
	return 0;
}
#endif

long
frameSpace(ptr)
	void *ptr;
{
	if (isStack(ptr))
	{
		return (long) ( (long)getFrame(1+stackLevel(ptr)) - (long)ptr - 14); // X86
	}
	return -1; /* unknown take as ok */
}
