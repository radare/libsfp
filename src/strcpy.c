#include "lib.h"

void *
strcpy(dest,src,n)
	void *dest;
	const void *src;
	size_t n;
{
	if (__lib_work)
	{
		if (frameSpace(dest)<n)
		{
			printf("BOF DETECTED INSIDE - %x\n",frameSpace(dest));
			return 0;
		}
	}
	return r_memcpy(dest,src,n);
#if 0	
	if ( isStack(dest) ) // local var
	{
		i=stackLevel(dest);
	//	printf("StackLevel  %d %x %x\n",i,getFrame(1),getFrame(2));

		if (dest+n+14>getFrame(1+stackLevel(dest))) // get frameadd of up frame
		{
		printf("Buffer overflow attempt!\n");
		return 0;
		}
	}
#endif
	//return r_memcpy(dest,src,n);
	return 0;
}
