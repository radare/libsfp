/* XXX malloc redefinition (we must sign every chunk with magic or take a db)
 *
 * TODO ->
 */

#include "lib.h"

void __libsfp_domagic(void *ptr)
{
	int *magic;
	if (ptr)
	{
		magic=(int *)ptr-2;
		*magic=0xDeAdBaBe;
	}
}

void *calloc(size_t nmemb, size_t size)
{
	void *ptr;

	ptr=___calloc(nmemb,size);
	__libsfp_domagic(ptr);

	return ptr;
}

void *malloc(size_t size)
{
	void *ptr;
	int *magic;

	if (size==0) size++;
	ptr=___malloc(size);
	__libsfp_domagic(ptr);

	return ptr;
}
