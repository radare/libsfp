#define _GNU_SOURCE
#include <fcntl.h>
#include <stdio.h>
#include <dlfcn.h>

extern int * (*___sysopen) (const char *,int,mode_t);
extern void * (*___memcpy) (void*,const char *,size_t);
extern void * (*___memset) (void*,int,size_t);
extern size_t (*___strlen) (const char *);
extern void * (*___malloc) (size_t);
extern void * (*___calloc) (size_t,size_t);
extern int __lib_work;
