main()
{
	char buf[10];

	printf("==> memset(10/10)\n");
	memset(buf,'A',10);

	printf("==> memset(11/10)\n");
	memset(buf,'A',11);

	printf("==> memset(1024/10)\n");
	memset(buf,'A',1024);
}
