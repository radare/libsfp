#define _GNU_SOURCE
#include <dlfcn.h>
		
int
printf(const char *arg,...)
{
		int (*r_printf) (const char *,...);
		write(1,"<-->",4);
		r_printf=dlsym(RTLD_NEXT,"printf");
		r_printf(arg);
}
